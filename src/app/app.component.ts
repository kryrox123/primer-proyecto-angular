import { Component } from '@angular/core';

@Component({
  selector: 'app-root', //es el nombre del componente, de toda esta ventana.
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
}) //decorador
export class AppComponent { //clase
  title = 'my-app';
  nombre = 'Alba';
  apellido = 'León Roque'
}
